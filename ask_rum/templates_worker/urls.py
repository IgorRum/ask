from django.conf.urls import include, url, patterns
from templates_worker import views

urlpatterns = patterns('',
	url(r'^$', views.index),
	url(r'^signup/$', views.signup),
	url(r'^login/$', views.login),
)