from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

@csrf_exempt
def index(request):
	return render(request, 'index.html')

def signup(request):
	return render(request, 'signup.html')

def login(request):
	return render(request, 'login.html')
