from django.http import HttpResponse, HttpRequest
from django.views.decorators.csrf import csrf_exempt

@csrf_exempt


def hello(request): 
	return HttpResponse( stream_response(request), content_type = "text/plain")
	

def stream_response(request):
	yield "Hello World!\n\n"

	if request.method == 'GET':
		yield "GET\n"
		GetDict = request.GET
		for (param, value) in GetDict.items():
			yield param + '=' + GetDict.get(param) + '\n'

	elif request.method == 'POST':
		yield "POST\n"
		GetDict = request.POST
		for (param, value) in GetDict.items():
			yield param + '=' + GetDict.get(param) + '\n'
