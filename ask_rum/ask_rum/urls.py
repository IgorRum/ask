from django.conf.urls import include, url, patterns
from django.contrib import admin

import views
urlpatterns = [
    # Examples:
    # url(r'^$', 'ask_rum.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^helloworld/', views.hello, name='hello'),
    url(r'', include('templates_worker.urls')),
]


