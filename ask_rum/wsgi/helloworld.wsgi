from cgi import parse_qs 

def application(environ, start_response):
    status = '200 OK' 
    output = ['Hello World!\n\n']

    if environ['REQUEST_METHOD'] == 'GET':
        output.append('GET:\n')
        toparse = environ['QUERY_STRING']
        parsed = parse_qs(toparse)
    
        for (param, value) in parsed.items():
            output.append(param + '=' + parsed.get(param, [''])[0] + '\n')

    if environ['REQUEST_METHOD'] == 'POST':
        output.append('POST:\n')
        output.append(environ['wsgi.input'].read())   
    

    output_len = sum(len(line) for line in output)
    response_headers = [ ('Content-Type', 'text/plain'), 
                         ('Content-Length', str(output_len))]

    start_response(status, response_headers)
 
    return output
