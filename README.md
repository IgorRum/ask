#README#
Румянцев Игорь. Технопарк гр. АПО-13. Web-технологии ДЗ№1,2

* [Конфиг apache](https://bitbucket.org/IgorRum/ask/src/02f49a752519e2471e584f4b086f2af29b78a56a/apache2/?at=master)
* [Конфиг nginx](https://bitbucket.org/IgorRum/ask/src/fa3aee1bc05644217f176df715a0a54b63db2ace/nginx/?at=master)
* [Директория проекта](https://bitbucket.org/IgorRum/ask/src/1b965b327d2f7746dab1276028e6e7a577a07df7/ask_rum/?at=master)
* [Результаты нагрузочного тестирования](https://bitbucket.org/IgorRum/ask/src/d399f691379e98e457d57b43be8c722be4581871/bencmarks/?at=master)
* [Директория с шаблонами](https://bitbucket.org/IgorRum/ask/src/a23e0812537143a151911ed3b114d4152ae8fe2e/ask_rum/templates/?at=master)
* [Директория со статикой](https://bitbucket.org/IgorRum/ask/src/fa3aee1bc05644217f176df715a0a54b63db2ace/ask_rum/static/?at=master)